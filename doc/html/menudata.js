var menudata={children:[
{text:'Main Page',url:'index.html'},
{text:'Namespaces',url:'namespaces.html',children:[
{text:'Namespace List',url:'namespaces.html'},
{text:'Namespace Members',url:'namespacemembers.html',children:[
{text:'All',url:'namespacemembers.html'},
{text:'Variables',url:'namespacemembers_vars.html'},
{text:'Typedefs',url:'namespacemembers_type.html'},
{text:'Enumerator',url:'namespacemembers_eval.html'}]}]},
{text:'Classes',url:'annotated.html',children:[
{text:'Class List',url:'annotated.html'},
{text:'Class Index',url:'classes.html'},
{text:'Class Hierarchy',url:'inherits.html'},
{text:'Class Members',url:'functions.html',children:[
{text:'All',url:'functions.html',children:[
{text:'a',url:'functions.html#index_a'},
{text:'c',url:'functions_c.html#index_c'},
{text:'g',url:'functions_g.html#index_g'},
{text:'i',url:'functions_i.html#index_i'},
{text:'m',url:'functions_m.html#index_m'},
{text:'o',url:'functions_o.html#index_o'},
{text:'r',url:'functions_r.html#index_r'},
{text:'s',url:'functions_s.html#index_s'},
{text:'t',url:'functions_t.html#index_t'},
{text:'v',url:'functions_v.html#index_v'},
{text:'~',url:'functions_0x7e.html#index_0x7e'}]},
{text:'Functions',url:'functions_func.html',children:[
{text:'a',url:'functions_func.html#index_a'},
{text:'c',url:'functions_func_c.html#index_c'},
{text:'g',url:'functions_func_g.html#index_g'},
{text:'i',url:'functions_func_i.html#index_i'},
{text:'m',url:'functions_func_m.html#index_m'},
{text:'o',url:'functions_func_o.html#index_o'},
{text:'r',url:'functions_func_r.html#index_r'},
{text:'s',url:'functions_func_s.html#index_s'},
{text:'t',url:'functions_func_t.html#index_t'},
{text:'~',url:'functions_func_0x7e.html#index_0x7e'}]},
{text:'Typedefs',url:'functions_type.html'}]}]},
{text:'Files',url:'files.html',children:[
{text:'File List',url:'files.html'},
{text:'File Members',url:'globals.html',children:[
{text:'All',url:'globals.html'},
{text:'Macros',url:'globals_defs.html'}]}]}]}
